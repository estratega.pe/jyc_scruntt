<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->load->model('dashboard/activityModel', 'activity');
		$this->load->model('dashboard/activitytypeModel', 'at');
		$this->load->model('dashboard/conceptModel', 'concept');
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/datatable/datatable.css">';
		$this->args['EXTRA_SCRIPTS'] = '';
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'ACTIVIDAD']
				,['LABEL' => 'TIPO']
				,['LABEL' => 'ACCIONES']
		];
		$this->args['CONTENT_BODY'] = $this->parser->parse('ajax-table', ['BASE_URL' => base_url()
				,'BODY_TITLE'		=> 'Actividad'
				,'URL_AJAX'			=> base_url() . 'dashboard/activity/getActivity/datatables'
				,'BODY_DESCRIPTION'	=> 'Listado de Actividades'
				,'BODY_SUBTITLE'	=> ''
				,'TARGETS'			=> count($columnas)
				,'ID_TARGET'		=> 'activity_id'
				,'COLUMNAS'			=> json_encode($columnas)
				,'BODY_MENU'		=> createLink(base_url() . 'dashboard/activity/addActivity', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
				,'TH_TABLE'			=> $columnas
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function addActivity()
	{
		$activity_type_id = '';
		
		$select_activity_type = ['' => 'Seleccione'];
		$s = $this->at->getAt();
		foreach($s as $k) {
			$select_activity_type[$k->activity_type_id] = $k->activity_type_name;
		}
		
		if ( $this->input->post() )
		{
			$activity_name = $this->input->post('input_activity', TRUE);
			$activity_type = $this->input->post('select_activity_type');
			if( !empty($activity_name) AND !empty($activity_type) )
			{
				$values = [];
				$values['activity_name']	= strtoupper(strtolower($activity_name));
				$values['activity_machine']	= generateMachineName($activity_name);
				$values['activity_type_activity_type_id'] = $activity_type;
				$newID = $this->activity->saveActivity($values);
				if( !is_null($newID ) )
				{
					redirect( base_url() .'dashboard/activity/viewActivity/' . $newID);
				}
			}
		}
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/activity/addActivity', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Agregar Actividad'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/activity/addActivity'
				,'INPUT_ACTIVITY'			=> form_input('input_activity', '', 'class="form-control" required')
				,'SELECT_ACTIVITY_TYPE'		=> form_dropdown(array('name' => 'select_activity_type', 'id' => 'select_activity_type'), $select_activity_type, $activity_type_id, 'class="span6" required required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/activity', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	function viewActivity() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$c = $this->activity->getActivityRow(['activity_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$_selected_concept = $this->activity->getChA(['activity_activity_id' => $ID]);
		$selected_concept = [];
		foreach($_selected_concept as $s) {
			array_push($selected_concept, $s->concept_concept_id);
		}
		
		$select_concept = [];
		$listConcept = $this->concept->getConcept();
		foreach($listConcept as $r) {
			$checked = ( in_array($r->concept_id, $selected_concept) ) ? 'checked' : '';
			array_push($select_concept, [
					'CONCEPT_ID'	=> $r->concept_id
					,'CONCEPT_NAME'	=> $r->concept_name
					,'CHECKED'		=> $checked
			]);
		}
		$this->args['EXTRA_CSS']		= '';
		$this->args['EXTRA_SCRIPTS']	= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/activity/viewActivity', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Ver Actividad'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'ACTIVITY_ID'				=> $ID
				,'INPUT_ACTIVITY'			=> $c->activity_name
				,'SELECT_ACTIVITY_TYPE'		=> $c->activity_type_name
				,'LIST_CONCEPT'				=> $select_concept
				,'BUTTON_SUBMIT'			=> createLink(base_url() . 'dashboard/activity/editActivity/' . $c->activity_id, 'btn-success', 'icon-pencil', 'Editar', true)
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/activity', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	function deleteActivity() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$bg_color = '';
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n.';
		
		$c = $this->activity->getActivityRow(['activity_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		if( $this->input->post() ) {
			if( $this->input->post('input_activity_id') == $ID)
			{
				if( $this->activity->deleteActivity(['activity_id' => $ID]) === FALSE )
				{
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect( base_url() . 'dashboard/activity' );
				}
			}
		}
		
		$this->args['EXTRA_CSS']		= '';
		$this->args['EXTRA_SCRIPTS']	= '';
		$this->args['CONTENT_BODY']		= $this->parser->parse('deleteForm', [
				'BASE_URL'			=> base_url()
				,'BODY_TITLE'		=> 'Eliminar Actividad - ' . $c->activity_name
				,'BODY_SUBTITLE'	=> $subtitle
				,'BODY_MENU'		=> '(' . $c->activity_name . ')'
				,'BODY_DESCRIPTION'	=> 'Confirme eliminacion de la Actividad "' . $c->activity_name . '"'
				,'BG_COLOR'			=> $bg_color
				,'URL_POST'			=> base_url() . 'dashboard/activity/deleteActivity/' . $ID
				,'INPUT_DELETE_ID'	=> form_hidden('input_activity_id', $ID)
				,'BUTTON_SUBMIT'	=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
				,'BUTTON_CANCEL'	=> createLink(base_url() . 'dashboard/activity', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$this->parser->parse('layout', $this->args);
	}
	
	public function setConcept() {
		$activity_id = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$concept_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		
		$_selected_concept = $this->activity->getChA(['activity_activity_id' => $activity_id]);
		$selected_concept = [];
		foreach($_selected_concept as $s) {
			array_push($selected_concept, $s->concept_concept_id);
		}
		if( in_array($concept_id, $selected_concept)) {
			if( $this->activity->unsetConcept([
					'activity_activity_id' => $activity_id
					, 'concept_concept_id' => $concept_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		} else {
			if( $this->activity->setConcept([
					'activity_activity_id' => $activity_id
					, 'concept_concept_id' => $concept_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		}
	}
	
	public function getActivity() 
	{
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		
		if( $this->input->post() )
		{
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			
			if($this->input->post('sSearch', TRUE) != '')
			{
				$search = $this->input->post('sSearch');
				$like = ['activity_name' => $search];
				$result = $this->activity->getActivity(null, $like, $limit, $start, null);
				$total = $this->activity->getTotalActivity(null, $like, null, null, null);
				
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['activity_name' => $search];
					$result = $this->activity->getActivity(null, $like, $limit, $start, null);
					$total = $this->activity->getTotalActivity(null, $like, null, null, null);
				} else {
					$result = $this->activity->getActivity(null, null, $limit, $start);
					$total = $this->activity->getTotalActivity();
				}
				
			}
		} else {
			$result = $this->activity->getActivity();
		}
		
		switch ($format)
		{
			case 'datatables':
				$data = [];
				if($this->input->post())
				{
					$records = [];
					foreach( $result as $r) {
						$show = createLink(base_url() . 'dashboard/activity/viewActivity/' . $r->activity_id, 'btn-info', 'icon-eye', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'dashboard/activity/editActivity/' . $r->activity_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'dashboard/activity/deleteActivity/' . $r->activity_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						array_push($records, [
								'DT_RowId'	=> $r->activity_id
								,'DT_RowClass' => ''
								,0	=> $r->activity_id
								,1	=> $r->activity_name 
								,2	=> createLink(base_url() . 'dashboard/activity-type/viewActivity-Type/' . $r->activity_type_id, 'btn-info', 'icon-eye', $r->activity_type_name, true)
								,3	=> $link
						]);
					}
					
					$data = ['sEcho' => $sEcho
							,'iTotalRecords' => $total
							,'iTotalDisplayRecords' => $total
							,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
	}
}