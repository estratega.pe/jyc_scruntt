<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_type extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->load->model('dashboard/activitytypeModel', 'at');
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/datatable/datatable.css">';
		$this->args['EXTRA_SCRIPTS'] = '';
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'TIPO ACTIVIDAD']
				,['LABEL' => 'ACCIONES']
		];
		$this->args['CONTENT_BODY'] = $this->parser->parse('ajax-table', ['BASE_URL' => base_url()
				,'BODY_TITLE'		=> 'Tipo Actividad'
				,'URL_AJAX'			=> base_url() . 'dashboard/activity-type/getActivity-type/datatables'
				,'BODY_DESCRIPTION'	=> 'Listado de Tipos de Actividad'
				,'BODY_SUBTITLE'	=> ''
				,'TARGETS'			=> count($columnas)
				,'ID_TARGET'		=> 'activity_type_id'
				,'COLUMNAS'			=> json_encode($columnas)
				,'BODY_MENU'		=> createLink(base_url() . 'dashboard/activity-type/addActivity-type', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
				,'TH_TABLE'			=> $columnas
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function addActivity_type()
	{
		if ( $this->input->post() )
		{
			$activity_type_name = $this->input->post('input_activity_type', TRUE);
			if( !empty($activity_type_name) )
			{
				$values = [];
				$values['activity_type_name']	= strtoupper(strtolower($activity_type_name));
				$newID = $this->at->saveAt($values);
				if( !is_null($newID ) )
				{
					redirect( base_url() .'dashboard/activity-type');
				}
			}
		}
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/activity-type/addAt', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Agregar Tipo Actividad'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/activity-type/addActivity-type'
				,'INPUT_ACTIVITY_TYPE'		=> form_input('input_activity_type', '', 'class="form-control" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/activity-type', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	function viewActivity_Type() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$c = $this->at->getAtRow(['activity_type_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		
		
		$this->args['EXTRA_CSS']			= '';
		$this->args['EXTRA_SCRIPTS']		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/activity-type/viewAt', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Ver Tipo de Actividad'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'ACTIVITY_TYPE_ID'			=> $ID
				,'INPUT_ACTIVITY_TYPE_NAME'	=> $c->activity_type_name
				,'BUTTON_SUBMIT'			=> createLink(base_url() . 'dashboard/activity-type/editActivity-Type/' . $c->activity_type_id, 'btn-success', 'icon-pencil', 'Editar', true)
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/activity-type', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	function deleteActivity_type() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/activity-type');
			exit();
		}
		
		$bg_color = '';
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n.';
		
		$c = $this->at->getAtRow(['activity_type_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity-type');
			exit();
		}
		
		if( $this->input->post() ) {
			if( $this->input->post('input_activity_type_id') == $ID) 
			{
				if( $this->at->deleteAt(['activity_type_id' => $ID]) === FALSE ) 
				{
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect( base_url() . 'dashboard/activity-type' );
				}
			}
		}
		
		$this->args['EXTRA_CSS']		= '';
		$this->args['EXTRA_SCRIPTS']	= '';
		$this->args['CONTENT_BODY']		= $this->parser->parse('deleteForm', [
				'BASE_URL'			=> base_url()
				,'BODY_TITLE'		=> 'Eliminar Tipo de actividad - ' . $c->activity_type_name
				,'BODY_SUBTITLE'	=> $subtitle
				,'BODY_MENU'		=> strtoupper('( se eliminaran todas las actividades asociadas a este tipo de actividad )')
				,'BODY_DESCRIPTION'	=> 'Confirme eliminacion del Tipo de actividad "' . $c->activity_type_name . '"'
				,'BG_COLOR'			=> $bg_color
				,'URL_POST'			=> base_url() . 'dashboard/activity-type/deleteActivity-type/' . $ID
				,'INPUT_DELETE_ID'	=> form_hidden('input_activity_type_id', $ID)
				,'BUTTON_SUBMIT'	=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
				,'BUTTON_CANCEL'	=> createLink(base_url() . 'dashboard/activity-type', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$this->parser->parse('layout', $this->args);
	}
	
	public function getActivity_type() 
	{
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		
		if( $this->input->post() )
		{
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			
			if($this->input->post('sSearch', TRUE) != '')
			{
				$search = $this->input->post('sSearch');
				$like = ['activity_type_name' => $search];
				$result = $this->at->getAt(null, $like, $limit, $start, null);
				$total = $this->at->getTotalAt(null, $like, null, null, null);
				
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['activity_type_name' => $search];
					$result = $this->at->getAt(null, $like, $limit, $start, null);
					$total = $this->at->getTotalAt(null, $like, null, null, null);
				} else {
					$result = $this->at->getAt(null, null, $limit, $start);
					$total = $this->at->getTotalAt();
				}
				
			}
		} else {
			$result = $this->at->getAt();
		}
		
		switch ($format)
		{
			case 'datatables':
				$data = [];
				if($this->input->post())
				{
					$records = [];
					foreach( $result as $r) {
						$show = createLink(base_url() . 'dashboard/activity-type/viewActivity-type/' . $r->activity_type_id, 'btn-info', 'icon-eye', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'dashboard/activity-type/editActivity-type/' . $r->activity_type_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'dashboard/activity-type/deleteActivity-type/' . $r->activity_type_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						array_push($records, [
								'DT_RowId'	=> $r->activity_type_id
								,'DT_RowClass' => ''
								,0	=> $r->activity_type_id
								,1	=> $r->activity_type_name 
								,2	=> $link
						]);
					}
					
					$data = ['sEcho' => $sEcho
							,'iTotalRecords' => $total
							,'iTotalDisplayRecords' => $total
							,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
	}
}