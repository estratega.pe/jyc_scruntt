<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Concept extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->load->model('dashboard/conceptModel', 'concept');
		$this->load->model('dashboard/platformModel', 'platform');
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/datatable/datatable.css">';
		$this->args['EXTRA_SCRIPTS'] = '';
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'CONCEPTO']
				,['LABEL' => 'PLATAFORMA']
				,['LABEL' => 'ACCIONES']
		];
		$this->args['CONTENT_BODY'] = $this->parser->parse('ajax-table', ['BASE_URL' => base_url()
				,'BODY_TITLE'		=> 'Concepto'
				,'URL_AJAX'			=> base_url() . 'dashboard/concept/getConcept/datatables'
				,'BODY_DESCRIPTION'	=> 'Listado de Conceptos'
				,'BODY_SUBTITLE'	=> ''
				,'TARGETS'			=> count($columnas)
				,'ID_TARGET'		=> 'concept_id'
				,'COLUMNAS'			=> json_encode($columnas)
				,'BODY_MENU'		=> createLink(base_url() . 'dashboard/concept/addConcept', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
				,'TH_TABLE'			=> $columnas
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function viewConcept() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/concept');
			exit();
		}
		
		$c = $this->concept->getConceptRow(['concept_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$_selected_platform = $this->concept->getConceptRelRow(['concept_concept_id' => $ID]);
		$selected_platform = [];
		
		foreach($_selected_platform as $s) {
			array_push($selected_platform, $s->platform_platform_id);
		}
		
		$select_platform = [];
		$listPlatform= $this->platform->getPlatform();
		foreach($listPlatform as $r) {
			$checked = ( in_array($r->platform_id, $selected_platform) ) ? 'checked' : '';
			array_push($select_platform, [
					'PLATFORM_ID'		=> $r->platform_id
					,'PLATFORM_NAME'	=> $r->platform_name
					,'CHECKED'			=> $checked
			]);
		}
		
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/concept/viewConcept', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Ver Concepto'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'CONCEPT_ID'				=> $ID
				,'INPUT_CONCEPT'			=> $c->concept_name
				,'LIST_PLATFORM'			=> $select_platform
				,'BUTTON_SUBMIT'			=> createLink(base_url() . 'dashboard/concept/editConcept/' . $c->concept_id, 'btn-success', 'icon-pencil', 'Editar', true)
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/concept', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function setPlatform() {
		$concept_id= (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$platform_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		
		$_selected_platform = $this->concept->getConceptRelRow(['concept_concept_id' => $concept_id]);
		$selected_platform = [];
		foreach($_selected_platform as $s) {
			array_push($selected_platform, $s->platform_platform_id);
		}
		
		if ( in_array($platform_id, $selected_platform) ) {
			if( $this->concept->unsetPlatform([
					'platform_platform_id' => $platform_id
					, 'concept_concept_id' => $concept_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		} else {
			if( $this->concept->setPlatform([
					'platform_platform_id' => $platform_id
					, 'concept_concept_id' => $concept_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		}
	}
	
	public function addConcept()
	{
		if ( $this->input->post() )
		{
			$concept_name = $this->input->post('input_concept', TRUE);
			if( !empty($concept_name) )
			{
				$values = [];
				$values['concept_name']	= strtoupper(strtolower($concept_name));
				$values['concept_machine']	= generateMachineName($concept_name);
				$newID = $this->concept->saveConcept($values);
				if( !is_null($newID ) )
				{
					redirect( base_url() .'dashboard/concept');
				}
			}
		}
		
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/concept/addConcept', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Agregar Concepto'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/concept/addConcept'
				,'INPUT_CONCEPT'			=> form_input('input_concept', '', 'class="form-control" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/concept', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	function deleteConcept() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/concept');
			exit();
		}
		
		$bg_color = '';
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n.';
		
		$c = $this->concept->getConceptRow(['concept_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/concept');
			exit();
		}
		
		if( $this->input->post() ) {
			if( $this->input->post('input_concept_id') == $ID)
			{
				if( $this->concept->deleteConcept(['concept_id' => $ID]) === FALSE )
				{
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect( base_url() . 'dashboard/concept' );
				}
			}
		}
		
		$this->args['EXTRA_CSS']		= '';
		$this->args['EXTRA_SCRIPTS']	= '';
		$this->args['CONTENT_BODY']		= $this->parser->parse('deleteForm', [
				'BASE_URL'			=> base_url()
				,'BODY_TITLE'		=> 'Eliminar Concepto - ' . $c->concept_name
				,'BODY_SUBTITLE'	=> $subtitle
				,'BODY_MENU'		=> '(' . $c->concept_name . ')'
				,'BODY_DESCRIPTION'	=> 'Confirme eliminacion del Concepto "' . $c->concept_name . '"'
				,'BG_COLOR'			=> $bg_color
				,'URL_POST'			=> base_url() . 'dashboard/concept/deleteConcept/' . $ID
				,'INPUT_DELETE_ID'	=> form_hidden('input_concept_id', $ID)
				,'BUTTON_SUBMIT'	=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
				,'BUTTON_CANCEL'	=> createLink(base_url() . 'dashboard/concept', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$this->parser->parse('layout', $this->args);
	}
	
	public function getConcept() 
	{
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		
		if( $this->input->post() )
		{
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			
			if($this->input->post('sSearch', TRUE) != '')
			{
				$search = $this->input->post('sSearch');
				$like = ['concept_name' => $search];
				$result = $this->concept->getConcept(null, $like, $limit, $start, null);
				$total = $this->concept->getTotalConcept(null, $like, null, null, null);
				
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['concept_name' => $search];
					$result = $this->concept->getConcept(null, $like, $limit, $start, null);
					$total = $this->concept->getTotalConcept(null, $like, null, null, null);
				} else {
					$result = $this->concept->getConcept(null, null, $limit, $start);
					$total = $this->concept->getTotalConcept();
				}
				
			}
		} else {
			$result = $this->concept->getConcept();
		}
		
		switch ($format)
		{
			case 'datatables':
				$data = [];
				if($this->input->post())
				{
					$records = [];
					foreach( $result as $r) {
						$show = createLink(base_url() . 'dashboard/concept/viewConcept/' . $r->concept_id, 'btn-info', 'icon-eye', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'dashboard/concept/editConcept/' . $r->concept_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'dashboard/concept/deleteConcept/' . $r->concept_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						$platforms = $this->concept->getRelConceptPlatform(['concept_concept_id' => $r->concept_id]);
						$platform = '';
						foreach($platforms as $p) {
							$platform .= '&nbsp;' . createLink(base_url() . 'dashboard/platform/viewPlatform/' . $p->platform_platform_id, 'btn-info', 'icon-eye', $this->platform->getPlatformRow(['platform_id' => $p->platform_platform_id])->platform_name, true);
						}
						$platform = (empty($platform)) ? 'Sin Asignar' : $platform;
						array_push($records, [
								'DT_RowId'	=> $r->concept_id
								,'DT_RowClass' => ''
								,0	=> $r->concept_id
								,1	=> $r->concept_name 
								,2	=> $platform
								,3	=> $link
						]);
					}
					
					$data = ['sEcho' => $sEcho
							,'iTotalRecords' => $total
							,'iTotalDisplayRecords' => $total
							,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
			default:
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $result ));
			break;
		}
	}
}