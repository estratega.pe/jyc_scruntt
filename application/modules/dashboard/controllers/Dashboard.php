<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '';
		$this->parser->parse('layout', $this->args);
	}
}