<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Platform extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->load->model('dashboard/platformModel', 'platform');
		$this->load->model('dashboard/conceptModel', 'concept');
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/datatable/datatable.css">';
		$this->args['EXTRA_SCRIPTS'] = '';
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'PLATAFORMA']
				,['LABEL' => 'ACCIONES']
		];
		$this->args['CONTENT_BODY'] = $this->parser->parse('ajax-table', ['BASE_URL' => base_url()
				,'BODY_TITLE'		=> 'Plataforma'
				,'URL_AJAX'			=> base_url() . 'dashboard/platform/getPlatform/datatables'
				,'BODY_DESCRIPTION'	=> 'Listado de Plataformas'
				,'BODY_SUBTITLE'	=> ''
				,'TARGETS'			=> count($columnas)
				,'ID_TARGET'		=> 'platform_id'
				,'COLUMNAS'			=> json_encode($columnas)
				,'BODY_MENU'		=> createLink(base_url() . 'dashboard/platform/addPlatform', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
				,'TH_TABLE'			=> $columnas
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function viewPlatform() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/concept');
			exit();
		}
		
		$c = $this->platform->getPlatformRow(['platform_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/activity');
			exit();
		}
		
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/platform/viewPlatform', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Ver Plataforma'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'CONCEPT_ID'				=> $ID
				,'INPUT_PLATFORM'			=> $c->platform_name
				,'BUTTON_SUBMIT'			=> createLink(base_url() . 'dashboard/platform/editPlatform/' . $c->platform_id, 'btn-success', 'icon-pencil', 'Editar', true)
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/platform', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function addPlatform()
	{
		if ( $this->input->post() )
		{
			$platform_name = $this->input->post('input_platform', TRUE);
			if( !empty($platform_name) )
			{
				$values = [];
				$values['platform_name']	= strtoupper(strtolower($platform_name));
				$values['platform_machine']	= generateMachineName(strtolower($platform_name));
				$newID = $this->platform->savePlatform($values);
				if( !is_null($newID ) )
				{
					redirect( base_url() .'dashboard/platform');
				}
			}
		}
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/platform/addPlatform', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Agregar Plataforma'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/platform/addPlatform'
				,'INPUT_PLATFORM'			=> form_input('input_platform', '', 'class="form-control" required')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/platform', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	function deletePlatform() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/platform');
			exit();
		}
		
		$bg_color = '';
		$subtitle = 'No se puede deshacer la eliminaci&oacute;n.';
		
		$c = $this->platform->getPlatformRow(['platform_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/platform');
			exit();
		}
		
		if( $this->input->post() ) {
			if( $this->input->post('input_platform_id') == $ID)
			{
				if( $this->platform->deletePlatform(['platform_id' => $ID]) === FALSE )
				{
					$subtitle = 'Lo sentimos en este momento no podemos procesar su solicitud, por favor intentelo m&aacute;s tarde.';
					$bg_color = 'red';
				} else {
					redirect( base_url() . 'dashboard/platform' );
				}
			}
		}
		
		$this->args['EXTRA_CSS']		= '';
		$this->args['EXTRA_SCRIPTS']	= '';
		$this->args['CONTENT_BODY']		= $this->parser->parse('deleteForm', [
				'BASE_URL'			=> base_url()
				,'BODY_TITLE'		=> 'Eliminar Plataforma - ' . $c->platform_name
				,'BODY_SUBTITLE'	=> $subtitle
				,'BODY_MENU'		=> ''//strtoupper('( se eliminaran todas las actividades asociadas a este tipo de actividad )')
				,'BODY_DESCRIPTION'	=> 'Confirme eliminacion de la plataforma "' . $c->platform_name . '"'
				,'BG_COLOR'			=> $bg_color
				,'URL_POST'			=> base_url() . 'dashboard/platform/deletePlatform/' . $ID
				,'INPUT_DELETE_ID'	=> form_hidden('input_platform_id', $ID)
				,'BUTTON_SUBMIT'	=> createSubmitButton('Eliminar', 'btn-success', 'icon-remove')
				,'BUTTON_CANCEL'	=> createLink(base_url() . 'dashboard/platform', 'btn-danger', 'icon-ban', 'Cancelar', true)
		], TRUE);
		$this->parser->parse('layout', $this->args);
	}
	
	public function getPlatform() 
	{
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		
		if( $this->input->post() )
		{
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			
			if($this->input->post('sSearch', TRUE) != '')
			{
				$search = $this->input->post('sSearch');
				$like = ['platform_name' => $search];
				$result = $this->platform->getPlatform(null, $like, $limit, $start, null);
				$total = $this->platform->getTotalPlatform(null, $like, null, null, null);
				
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['platform_name' => $search];
					$result = $this->platform->getPlatform(null, $like, $limit, $start, null);
					$total = $this->platform->getTotalPlatform(null, $like, null, null, null);
				} else {
					$result = $this->platform->getPlatform(null, null, $limit, $start);
					$total = $this->platform->getTotalPlatform();
				}
				
			}
		} else {
			$result = $this->platform->getPlatform();
		}
		
		switch ($format)
		{
			case 'return':
				return $result;
			break;
			case 'datatables':
				$data = [];
				if($this->input->post())
				{
					$records = [];
					foreach( $result as $r) {
						$show = createLink(base_url() . 'dashboard/platform/viewPlatform/' . $r->platform_id, 'btn-info', 'icon-eye', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'dashboard/platform/editPlatform/' . $r->platform_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'dashboard/platform/deletePlatform/' . $r->platform_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						
						array_push($records, [
								'DT_RowId'	=> $r->platform_id
								,'DT_RowClass' => ''
								,0	=> $r->platform_id
								,1	=> $r->platform_name 
								,2	=> $link
						]);
					}
					
					$data = ['sEcho' => $sEcho
							,'iTotalRecords' => $total
							,'iTotalDisplayRecords' => $total
							,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
		}
	}
	
	public function getConcept() {
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$platform_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		
		$result = $this->platform->getPlatformRelRow(['platform_platform_id' => $platform_id]);
		
		switch($format) 
		{
			case 'select':
				$data = '<option value="">Seleccione...</option>';
				foreach($result as $r) {
					$text = $this->concept->getConceptRow(['concept_id' => $r->concept_concept_id]);
					$data .= '<option value="' . $r->concept_concept_id . '">' . $text->concept_name . '</option>';
				}
				$this->output
				->set_content_type('application/html')
				->set_output( $data );
			break;
		}
		
	}
}