<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends MX_Controller {
	
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->load->model('dashboard/projectModel', 'project');
		$this->load->model('dashboard/customerModel', 'customer');
		$this->load->model('dashboard/platformModel', 'platform');
		$this->load->model('dashboard/conceptModel', 'concept');
		$this->load->model('dashboard/activityModel', 'activity');
		$this->load->model('dashboard/userModel', 'user');
	}
	
	public function index()
	{
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/datatable/datatable.css">';
		$this->args['EXTRA_SCRIPTS'] = '';
		$columnas = [
				['LABEL' => 'ID']
				,['LABEL' => 'PROYECTO']
				,['LABEL' => 'FECHA INICIO']
				,['LABEL' => 'FECHA FINAL']
				,['LABEL' => 'ACCIONES']
		];
		$this->args['CONTENT_BODY'] = $this->parser->parse('ajax-table', ['BASE_URL' => base_url()
				,'BODY_TITLE'		=> 'Proyectos'
				,'URL_AJAX'			=> base_url() . 'dashboard/project/getProject/datatables'
				,'BODY_DESCRIPTION'	=> 'Listado de Proyectos'
				,'BODY_SUBTITLE'	=> ''
				,'TARGETS'			=> count($columnas)
				,'ID_TARGET'		=> 'project_id'
				,'COLUMNAS'			=> json_encode($columnas)
				,'BODY_MENU'		=> createLink(base_url() . 'dashboard/project/addProject', 'btn-blue-alt', 'icon-plus', 'Nuevo', true)
				,'TH_TABLE'			=> $columnas
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function addProject()
	{
		if ( $this->input->post() )
		{
			$project_name = $this->input->post('input_project', TRUE);
			$project_f_start = $this->input->post('input_project_f_start', TRUE);
			$project_f_end = $this->input->post('input_project_f_end', TRUE);
			$jyc_cuystomer_customer_id = $this->input->post('select_customer');
			$platform_platform_id = $this->input->post('select_platform');
			if( !empty($project_name) )
			{
				$values = [];
				$values['project_name']	= strtoupper(strtolower($project_name));
				$values['project_f_start'] = ( empty($project_f_start) ) ? null : $project_f_start;
				$values['project_f_end'] = ( empty($project_f_end) ) ? null : $project_f_end;
				$values['jyc_customer_customer_id'] = $jyc_cuystomer_customer_id;
				$values['platform_platform_id'] = $platform_platform_id;
				$newID = $this->project->saveProject($values);
				if( !is_null($newID ) )
				{
					$phc = $this->project->getPhC(['platform_platform_id' => $platform_platform_id]);
					foreach ($phc as $r) {
						$cha = $this->project->getChA(['concept_concept_id' => $r->concept_concept_id]);
						foreach($cha as $c) {
							$this->project->activityToProject([
									'activity_activity_id'	=> $c->activity_activity_id
									,'project_project_id'	=> $newID
									,'concept_concept_id'	=> $r->concept_concept_id
									
							]);
						}
					}
					redirect( base_url() .'dashboard/project/viewProject/' . $newID);
				}
			}
		}
		
		$rCustomer = modules::run('dashboard/customer/getCustomer', 'return');
		$selectCustomer = [];
		$selectCustomer[] = 'Seleccione...';
		foreach($rCustomer as $r) {
			$selectCustomer[$r->customer_id] = $r->customer_name;
		}
		
		$rPlatform = modules::run('dashboard/platform/getPlatform', 'return');
		$selectPlatform = [];
		$selectPlatform[] = 'Seleccione...'; 
		foreach($rPlatform as $r) {
			$selectPlatform[$r->platform_id] = $r->platform_name; 
		}
		
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/project/addProject', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Agregar Proyecto'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/project/addProject'
				,'INPUT_PROJECT'			=> form_input('input_project', '', 'class="form-control" required')
				,'INPUT_PROJECT_F_START'	=> form_input('input_project_f_start', '', 'class="bootstrap-datepicker form-control" data-date-format="yyyy-mm-dd"')
				,'INPUT_PROJECT_F_END'		=> form_input('input_project_f_end', '', 'class="bootstrap-datepicker form-control" data-date-format="yyyy-mm-dd"')
				,'SELECT_CUSTOMER'			=> form_dropdown(array('name' => 'select_customer', 'id' => 'select_customer'), $selectCustomer, '', 'class="span6" required ')
				,'SELECT_PLATFORM'			=> form_dropdown(array('name' => 'select_platform', 'id' => 'select_platform'), $selectPlatform, '', 'class="span6" required ')
				,'BUTTON_SUBMIT'			=> createSubmitButton('Grabar', 'btn-blue-alt', 'icon-save')
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/project', 'btn-danger', 'icon-ban', 'Cancelar', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function viewProject() {
		$ID = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$ID = filter_var(intval($ID),FILTER_VALIDATE_INT) ? $ID : 0;
		
		if( $ID == 0x0000 ) {
			redirect(base_url() . 'dashboard/project');
			exit();
		}
		
		$c = $this->project->getProjectRow(['project_id' => $ID]);
		
		if(count($c) <= 0x0000 || $c === FALSE) {
			redirect(base_url() . 'dashboard/project');
			exit();
		}
		$pha = $this->project->getProjectActivity(['project_project_id' => $c->project_id]);
		$selectedPhA = [];
		foreach($pha as $h) {
			$selectedPhA[$h->concept_concept_id][] = $h->activity_activity_id;
		}
		$phc = $this->project->getPhC(['platform_platform_id' => $c->platform_platform_id]);
		$lisConcept = [];
		foreach($phc as $p) {
			$_p = [];
			$concept = $this->concept->getConceptRow(['concept_id' => $p->concept_concept_id]);
			$_p['CONCEPT_NAME'] = $concept->concept_name;
			$activity = $this->activity->getAhC(['concept_concept_id' => $p->concept_concept_id]);
			$_a = [];
			foreach($activity as $r) {
				$a = $this->activity->getActivityRow(['activity_id' => $r->activity_activity_id]);
				$aConcept = (isset($selectedPhA[$p->concept_concept_id])) ? $selectedPhA[$p->concept_concept_id] : [];
				$checked = (in_array($r->activity_activity_id, $aConcept)) ? ' checked ' : '';
				array_push($_a, [
						'ACTIVITY_ID'		=> $a->activity_id
						,'ACTIVITY_NAME'	=> $a->activity_name
						,'CONCEPT_ID'		=> $concept->concept_id
						,'CHECKED'			=> $checked
				]);
			}
			$_p['LIST_ACTIVITY'] = $_a;
			array_push($lisConcept, $_p);
		}
		
		$sUser = $this->project->getUhP(['project_project_id' => $ID]);
		$selected_user = [];
		foreach($sUser as $u) {
			$selected_user[] = $u->jyc_user_user_id;
		}
		$listUser = [];
		$users = $this->user->getUser(['user_status' => '2']);
		foreach($users as $u) {
			$checked = (in_array($u->user_id, $selected_user)) ? ' checked ' : '';
			array_push($listUser, [
					'USER_ID'		=> $u->user_id
					,'USER_NAME'	=> $u->user_firstname . ' ' . $u->user_lastname
					,'CHECKED'		=> $checked
			]);
		}
		$this->args['EXTRA_CSS'] 			='';
		$this->args['EXTRA_SCRIPTS'] 		= '';
		$this->args['CONTENT_BODY'] 		= $this->parser->parse('dashboard/project/viewProject', [
				'BASE_URL'					=> base_url()
				,'BODY_TITLE'				=> 'Ver Proyecto'
				,'BODY_SUBTITLE'			=> ''
				,'BODY_MENU'				=> ''
				,'URL_POST'					=> base_url() . 'dashboard/project/addProject'
				,'PROJECT_ID'				=> $c->project_id
				,'INPUT_PROJECT'			=> $c->project_name
				,'INPUT_PROJECT_F_START'	=> (is_null($c->project_f_start)) ? '' : date('d-m-Y', strtotime($c->project_f_start))
				,'INPUT_PROJECT_F_END'		=> (is_null($c->project_f_end)) ? '' : date('d-m-Y', strtotime($c->project_f_end))
				,'SELECT_CUSTOMER'			=> strtoupper($this->customer->getCustomerRow(['customer_id' => $c->jyc_customer_customer_id])->customer_name)
				,'SELECT_PLATFORM'			=> strtoupper($this->platform->getPlatformRow(['platform_id' => $c->platform_platform_id])->platform_name)
				,'LIST_CONCEPT'				=> $lisConcept
				,'LIST_USER'				=> $listUser
				,'BUTTON_SUBMIT'			=> createLink(base_url() . 'dashboard/project/editProject/' . $c->project_id, 'btn-success', 'icon-pencil', 'Editar', true)
				,'BUTTON_CANCEL'			=> createLink(base_url() . 'dashboard/project', 'btn-blue-alt', 'icon-list', 'Volver al listado', true)
				
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function setActivity() {
		$project_id = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$concept_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		$activity_id = (sizeof(func_get_args()) >= 0x0003) ? func_get_arg(2): $this->uri->segment(6);
		
		$pha = $this->project->getProjectActivity(['project_project_id' => $project_id]);
		$selectedPhA = [];
		foreach($pha as $h) {
			$selectedPhA[$h->concept_concept_id][] = $h->activity_activity_id;
		}
		$concept_selected = (isset($selectedPhA[$concept_id])) ? $selectedPhA[$concept_id] : [];
		if( in_array($activity_id, $concept_selected)) {
			if( $this->project->unsetActivity([
					'activity_activity_id' => $activity_id
					,'concept_concept_id' => $concept_id
					,'project_project_id' => $project_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		} else {
			if( $this->project->setActivity([
					'activity_activity_id' => $activity_id
					, 'concept_concept_id' => $concept_id
					,'project_project_id' => $project_id]) === FALSE) {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
					} else {
						$this->output
						->set_content_type('application/json')
						->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
					}
		}
	}
	
	public function setUser() {
		$project_id = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$user_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		
		$sUser = $this->project->getUhP(['project_project_id' => $project_id]);
		$selected_user = [];
		foreach($sUser as $u) {
			$selected_user[] = $u->jyc_user_user_id;
		}
		if( in_array($user_id, $selected_user) ) {
			if( $this->project->unsetUser([
					'jyc_user_user_id'		=> $user_id
					,'project_project_id'	=> $project_id
			]) === FALSE) {
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
			} else {
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
			}
		} else {
			if( $this->project->setUser([
					'jyc_user_user_id'		=> $user_id
					,'project_project_id'	=> $project_id
			]) === FALSE) {
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
			} else {
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
			}
		}
	}
	
	public function getProject() 
	{
		$format = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		
		if( $this->input->post() )
		{
			$total = 0;
			$start = $this->input->post('iDisplayStart');
			$limit = $this->input->post('iDisplayLength');
			$sEcho = $this->input->post('sEcho');
			
			if($this->input->post('sSearch', TRUE) != '')
			{
				$search = $this->input->post('sSearch');
				$like = ['project_name' => $search];
				$result = $this->project->getProject(null, $like, $limit, $start, null);
				$total = $this->project->getTotalProject(null, $like, null, null, null);
				
			} else {
				if($this->input->post('q') != '') {
					$search = $this->input->post('q');
					$like = ['project_name' => $search];
					$result = $this->project->getProject(null, $like, $limit, $start, null);
					$total = $this->project->getTotalProject(null, $like, null, null, null);
				} else {
					$result = $this->project->getProject(null, null, $limit, $start);
					$total = $this->project->getTotalProject();
				}
				
			}
		} else {
			$result = $this->project->getProject();
		}
		
		switch ($format)
		{
			case 'datatables':
				$data = [];
				if($this->input->post())
				{
					$records = [];
					foreach( $result as $r) {
						$show = createLink(base_url() . 'dashboard/project/viewProject/' . $r->project_id, 'btn-info', 'icon-eye', 'Ver');
						$edit = '&nbsp;' . createLink(base_url() . 'dashboard/project/editProject/' . $r->project_id, 'btn-success', 'icon-pencil', 'Editar');
						$del = '&nbsp;' . createLink(base_url() . 'dashboard/project/deleteProject/' . $r->project_id, 'btn-danger', 'icon-trash', 'Eliminar');
						$link = $show . $edit . $del;
						
						array_push($records, [
								'DT_RowId'	=> $r->project_id
								,'DT_RowClass' => ''
								,0	=> $r->project_id
								,1	=> $r->project_name
								,2	=> (is_null($r->project_f_start) ) ? '' : date('d/m/Y', strtotime($r->project_f_start))
								,3	=> (is_null($r->project_f_end) ) ? '' : date('d/m/Y', strtotime($r->project_f_end)) 
								,4	=> $link
						]);
					}
					
					$data = ['sEcho' => $sEcho
							,'iTotalRecords' => $total
							,'iTotalDisplayRecords' => $total
							,'aaData' => $records
					];
				}
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode( $data ));
			break;
		}
	}
}