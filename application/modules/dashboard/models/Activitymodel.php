<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class activityModel extends MX_Controller {
	
	const ACTIVITY = 'activity';
	const ACTIVITY_TYPE = 'activity_type';
	const C_H_A = 'concept_has_activity';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveActivity($data) {
		$this->db->trans_begin();
		$this->db->insert(self::ACTIVITY, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function getActivity( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::ACTIVITY);
		$this->db->join(self::ACTIVITY_TYPE, self::ACTIVITY_TYPE . '.activity_type_id= ' . self::ACTIVITY . '.activity_type_activity_type_id');
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalActivity( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::ACTIVITY);
		$this->db->join(self::ACTIVITY_TYPE, self::ACTIVITY_TYPE . '.activity_type_id= ' . self::ACTIVITY. '.activity_type_activity_type_id');
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
			
		return $this->db->count_all_results();
	}
	
	function getActivityRow($where = null) {
		$this->db->from(self::ACTIVITY);
		$this->db->join(self::ACTIVITY_TYPE, self::ACTIVITY_TYPE . '.activity_type_id=' . self::ACTIVITY . '.activity_type_activity_type_id');
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function deleteActivity($where) {
		$this->db->trans_begin();
		$this->db->delete(self::ACTIVITY, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	function getChA($where =null) {
		$this->db->select('concept_concept_id');
		$this->db->from(self::C_H_A);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function getAhC($where =null) {
		$this->db->from(self::C_H_A);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function setConcept($data) {
		$this->db->trans_begin();
		$this->db->insert(self::C_H_A, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function unsetConcept($where) {
		$this->db->trans_begin();
		$this->db->delete(self::C_H_A, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
}