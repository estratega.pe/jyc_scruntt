<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class activitytypeModel extends MX_Controller {
	
	const ACTIVITY = 'activity';
	const AT = 'activity_type';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveAt($data) {
		$this->db->trans_begin();
		$this->db->insert(self::AT, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function getAt( $where = null, $like = null, $limit = null, $start = null, $or_like = null)
	{
		$this->db->from(self::AT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalAt( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::AT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
			
		return $this->db->count_all_results();
	}
	
	function getAtRow($where = null) {
		$this->db->from(self::AT);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function deleteAt($where) {
		$this->db->trans_begin();
		$this->db->delete(self::ACTIVITY, ['activity_type_activity_type_id' => $where['activity_type_id']]);
		$this->db->delete(self::AT, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
}