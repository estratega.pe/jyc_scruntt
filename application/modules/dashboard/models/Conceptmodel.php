<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class conceptModel extends MX_Controller {
	
	const CONCEPT = 'concept';
	const PLATFORM = 'platform';
	const PLATFORM_REL = 'platform_has_concept';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveConcept($data) {
		$this->db->trans_begin();
		$this->db->insert(self::CONCEPT, $data);
		$ID = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function deleteConcept($where) {
		$this->db->trans_begin();
		$this->db->delete(self::PLATFORM_REL, ['concept_concept_id' => $where['concept_id']]);
		$this->db->delete(self::CONCEPT, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	function getConcept( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::CONCEPT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalConcept( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::CONCEPT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
			
		return $this->db->count_all_results();
	}
	
	function getRelConceptPlatform($where = null) {
		$this->db->from(self::PLATFORM_REL);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function getConceptRow($where = null) {
		$this->db->from(self::CONCEPT);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function getConceptRelRow($where = null) {
		$this->db->from(self::PLATFORM_REL);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function setPlatform($data) {
		$this->db->trans_begin();
		$this->db->insert(self::PLATFORM_REL, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function unsetPlatform($where) {
		$this->db->trans_begin();
		$this->db->delete(self::PLATFORM_REL, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
}