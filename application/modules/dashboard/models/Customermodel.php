<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class customerModel extends MX_Controller {
	const CUSTOMER = 'jyc_customer';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getCustomerRow($where = null) {
		$this->db->from(self::CUSTOMER);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function saveCustomer($data) {
		$this->db->trans_begin();
		$this->db->insert(self::CUSTOMER, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function getCustomer( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::CUSTOMER);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalCustomer( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::CUSTOMER);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->count_all_results();
	}
}