<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class projectModel extends MX_Controller {
	
	const PROJECT = 'project';
	const PHC = 'platform_has_concept';
	const CHA = 'concept_has_activity';
	const AHP = 'activity_has_project';
	const USER = 'jyc_user';
	const UHP = 'jyc_user_has_project';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getProjectActivity($where) {
		$this->db->from(self::AHP);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function getChA($where = null) {
		$this->db->from(self::CHA);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function getPhC($where = null) {
		$this->db->from(self::PHC);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
	function getProjectRow($where = null) {
		$this->db->from(self::PROJECT);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get_where()->row();
	}
	
	function activityToProject($data) {
		$this->db->trans_begin();
		$this->db->insert(self::AHP, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function saveProject($data) {
		$this->db->trans_begin();
		$this->db->insert(self::PROJECT, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function getProject( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::PROJECT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
		
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
		
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalProject( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::PROJECT);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
			
		return $this->db->count_all_results();
	}
	
	function setActivity($data) {
		$this->db->trans_begin();
		$this->db->insert(self::AHP, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function unsetActivity($where) {
		$this->db->trans_begin();
		$this->db->delete(self::AHP, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	function setUser($data) {
		$this->db->trans_begin();
		$this->db->insert(self::UHP, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
	function unsetUser($where) {
		$this->db->trans_begin();
		$this->db->delete(self::UHP, $where);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	function getUhP($where) {
		$this->db->from(self::UHP);
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
		return $this->db->get()->result();
	}
	
}