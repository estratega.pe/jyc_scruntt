<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userModel extends MX_Controller {
	
	const USER = 'jyc_user';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getUser( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::USER);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->get()->result();
	}
	
	function getTotalUser( $where = null, $like = null, $limit = null, $start = null, $or_like = null )
	{
		$this->db->from(self::USER);
		
		if(!is_null($where) AND is_array($where))
			$this->db->where($where);
			
		if(!is_null($like) AND is_array($like)) {
			$this->db->group_start();
			$this->db->like($like);
			if(is_null($or_like) AND !is_array($or_like))
				$this->db->group_end();
		}
			
		if(!is_null($or_like) AND is_array($or_like)) {
			if(is_null($like) AND !is_array($like))
				$this->db->group_start();
			$this->db->or_like($or_like);
			$this->db->group_end();
		}
			
		if(!is_null($start) AND !is_null($limit))
			$this->db->limit($limit, $start);
				
		return $this->db->count_all_results();
	}
}