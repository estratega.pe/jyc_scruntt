<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel">
    <div class="panel-body">
		<div class="example-box-wrapper">
			<form method="post" action="{URL_POST}" enctype="multipart/form-data">
				<div class="row">
                    <div class="col-md-12 form-horizontal bordered-row">
						<div class="form-group">
                            <label class="col-sm-3 control-label">ACTIVIDAD</label>
                            <div class="col-sm-6">
                                {INPUT_ACTIVITY}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-3 control-label">TIPO ACTIVIDAD</label>
                            <div class="col-sm-6">
                                {SELECT_ACTIVITY_TYPE}
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-2">
							</div>
							<div class="col-sm-5">
                                {BUTTON_SUBMIT}
                            </div>
							<div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/select2/css/select2.min.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    jQuery(function() { "use strict";
        jQuery('.bootstrap-datepicker').bsdatepicker({
            format: 'yyyy-mm-dd'
        }).on('changeDate', function(e){
        	jQuery(this).bsdatepicker('hide');
        });
        $("#select_activity_type").select2();
    });

</script>
