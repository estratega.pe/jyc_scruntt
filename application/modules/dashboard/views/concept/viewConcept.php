<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel">
    <div class="panel-body">
		<div class="example-box-wrapper">
				<div class="row">
                    <div class="col-md-12 form-horizontal bordered-row">
						<div class="form-group">
                            <label class="col-sm-3 control-label">CONCEPTO</label>
                            <div class="col-sm-6">
                                {INPUT_CONCEPT}
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-2">
							</div>
							<div class="col-sm-5">
                                {BUTTON_SUBMIT}
                            </div>
							<div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>

<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			<div class="col-md-12 form-horizontal bordered-row">
				{LIST_PLATFORM}
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<input class="custom-checkbox" data-id="{PLATFORM_ID}" id="{CONCEPT_ID}" name="{PLATFORM_ID}" type="checkbox" {CHECKED}>
						</span>
						<input type="text" class="form-control" value="{PLATFORM_NAME}" readonly disabled>
					</div>
				</div>
				{/LIST_PLATFORM}
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/uniform/uniform.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.js"></script>

<script>
jQuery('.custom-checkbox').uniform();
jQuery('.checker span').append('<i class="glyph-icon icon-check"></i>');
jQuery('.custom-checkbox').on('click', function(){
	var ID = jQuery(this).attr('data-id');
	jQuery.ajax('{BASE_URL}dashboard/concept/setPlatform/{CONCEPT_ID}/' + ID).done(function(data){
		jQuery.jGrowl(data.msg, {
            sticky: false,
            position: 'top-right',
            theme: data.theme
        });
	});
});
</script>