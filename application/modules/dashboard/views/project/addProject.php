<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel">
    <div class="panel-body">
		<div class="example-box-wrapper">
			<form method="post" action="{URL_POST}" enctype="multipart/form-data">
				<div class="row">
                    <div class="col-md-12 form-horizontal bordered-row">
						<div class="form-group">
                            <label class="col-sm-3 control-label">PROYECTO/CONTRATO</label>
                            <div class="col-sm-6">
                                {INPUT_PROJECT}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-3 control-label">CLIENTE</label>
                            <div class="col-sm-6">
                                {SELECT_CUSTOMER}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-3 control-label">PLATAFORMA</label>
                            <div class="col-sm-6">
                                {SELECT_PLATFORM}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-3 control-label">PROYECTO INICIO</label>
                            <div class="col-sm-6">
                                {INPUT_PROJECT_F_START}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-3 control-label">PROYECTO FINAL</label>
                            <div class="col-sm-6">
                                {INPUT_PROJECT_F_END}
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-2">
							</div>
							<div class="col-sm-5">
                                {BUTTON_SUBMIT}
                            </div>
							<div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/select2/css/select2.min.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/chosen/chosen-demo.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datepicker/datepicker.js"></script>
<script type="text/javascript">
    /* Datepicker bootstrap */

    jQuery(function() {
        //"use strict";
        jQuery('.bootstrap-datepicker').bsdatepicker({
            format: 'yyyy-mm-dd'
        }).on('changeDate', function(e){
        	jQuery(this).bsdatepicker('hide');
        });
        jQuery("#select_customer").select2();
        jQuery("#select_platform").select2();
        jQuery("#select_platform").on('change', function(){
            var sPlatform = jQuery(this).val();
            jQuery.ajax('{BASE_URL}dashboard/platform/getConcept/select/' + sPlatform).done(function(data){
        		jQuery("#select_concept").html(data);
        		jQuery("#select_concept").prop('disabled', false);
        		jQuery("#select_concept").select2();
        	});
		});
    });

</script>
