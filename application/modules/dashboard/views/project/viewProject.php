<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>
<div class="row">
	<div class="col-md-6">
		<div class="dashboard-box dashboard-box-chart bg-white content-box">
			<div class="content-wrapper">
				<div class="header size-md float-left">
					<span>DETALLES DEL PROYECTO</span>
				</div>
				
				<div class="row list-grade form-horizontal">
					<div class="bordered-row">
						<div class="form-group">
                            <label class="col-sm-4 control-label">PROYECTO/CONTRATO</label>
                            <div class="col-sm-8">
                                {INPUT_PROJECT}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-4 control-label">CLIENTE</label>
                            <div class="col-sm-8">
                                {SELECT_CUSTOMER}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-4 control-label">PLATAFORMA</label>
                            <div class="col-sm-8">
                                {SELECT_PLATFORM}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-4 control-label">PROYECTO INICIO</label>
                            <div class="col-sm-8">
                                {INPUT_PROJECT_F_START}
                            </div>
						</div>
						<div class="form-group">
                            <label class="col-sm-4 control-label">PROYECTO FINAL</label>
                            <div class="col-sm-8">
                                {INPUT_PROJECT_F_END}
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-sm-2">
							</div>
							<div class="col-sm-5">
                                {BUTTON_SUBMIT}
                            </div>
							<div class="col-sm-4">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="dashboard-box dashboard-box-chart bg-white content-box">
			<div class="content-wrapper">
				<div class="size-md float-left">
					<span>MIEMBROS DEL PROYECTO</span>
				</div>
				<div class="form">
					{LIST_USER}
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
								<input class="custom-checkbox user-checkbox" data-id="{USER_ID}" id="{USER_ID}" name="{USER_ID}" type="checkbox" {CHECKED}>
							</span>
							<input type="text" class="form-control" value="{USER_NAME}" readonly disabled>
						</div>
					</div>
					{/LIST_USER}
				</div>
			</div>
		</div>
	</div>
</div>

{LIST_CONCEPT}
<!-- <div id="page-title">
    <h2>{CONCEPT_NAME}</h2>
</div> -->
<div class="panel">
	<div class="panel-body">
		<div class="example-box-wrapper">
			<div class="col-md-12 form-horizontal bordered-row">
				<h3>{CONCEPT_NAME}</h3>
				{LIST_ACTIVITY}
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<input class="custom-checkbox activity-checkbox" data-concept="{CONCEPT_ID}" data-id="{ACTIVITY_ID}" id="{ACTIVITY_ID}" name="{ACTIVITY_ID}" type="checkbox" {CHECKED}>
						</span>
						<input type="text" class="form-control" value="{ACTIVITY_NAME}" readonly disabled>
					</div>
				</div>
				{/LIST_ACTIVITY}
			</div>
		</div>
	</div>
</div>
{/LIST_CONCEPT}
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/uniform/uniform.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.css">
<script type="text/javascript" src="{BASE_URL}assets/widgets/uniform/uniform.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script>
	jQuery('.custom-checkbox').uniform();
	jQuery('.checker span').append('<i class="glyph-icon icon-check"></i>');
	jQuery('.activity-checkbox').on('click', function(){
		var ACTIVITY_ID = jQuery(this).attr('data-id');
		var CONCEPT_ID = jQuery(this).attr('data-concept');
		jQuery.ajax('{BASE_URL}dashboard/project/setActivity/{PROJECT_ID}/' + CONCEPT_ID + '/' + ACTIVITY_ID).done(function(data){
			jQuery.jGrowl(data.msg, {
	            sticky: false,
	            position: 'top-right',
	            theme: data.theme
	        });
		});
	});
	jQuery('.user-checkbox').on('click', function(){
		var USER_ID = jQuery(this).attr('data-id');
		jQuery.ajax('{BASE_URL}dashboard/project/setUser/{PROJECT_ID}/' + USER_ID).done(function(data){
			jQuery.jGrowl(data.msg, {
	            sticky: false,
	            position: 'top-right',
	            theme: data.theme
	        });
		});
	});
</script>