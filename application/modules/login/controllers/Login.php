<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	var $args;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = base_url();
	}
	
	public function index()
	{
		if($this->session->userdata('USER'))
			redirect(base_url());
		$this->args['URL_POST'] = base_url() . 'login/loginIn';
		$this->parser->parse('login/login', $this->args);
	}
	
	public function loginIn() 
	{
		if($this->input->post()) 
		{
			$user	= $this->security->xss_clean($this->input->post('user', TRUE));
			$password	= $this->security->xss_clean($this->input->post('password', TRUE));
			
			if(trim($user) == '' || trim($password) == '') 
			{
				redirect(base_url('login'));
				exit();
			} else {
				$usr = $this->_preLogin($user);
				if(sizeof($usr) == 0x0001) {
					if( crypt($password, $usr->user_salt) == $usr->user_passwd ) {
						$this->session->set_userdata('USER', $usr);
						$this->session->set_userdata('USERID', $usr->user_id);
						redirect( base_url() );
						exit();
					} else {
						redirect(base_url('login'));
						exit();
					}
				} else {
					redirect(base_url('login'));
					exit();
				}
			}
		} else {
			redirect(base_url('login'));
		}
	}
	
	public function loginOut()
	{
		$this->session->unset_userdata('USER');
		$this->session->unset_userdata('PERMS');
		$this->session->unset_userdata('USERID');
		redirect(base_url('login'));
	}
	
	public function register() {
		
	}
	
	public function recoveryPassword() {
		
	}
	
	public function resetPassword() {
		
	}
	
	public function createPassword($password = 123456)
	{
		$salt = $this->createSalt();
		$newPassword = crypt($password, $salt);
		echo $newPassword ."<br>".$salt;//return $newPassword;
	}
	
	public function createSalt()
	{
		$_crypt = '$6$rounds=' . mt_rand(1000, 10000) . '$';
		$_aString = './_-1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$salt = $_crypt;
		for($i = 0; $i < 25; $i++)
		{
			$salt .= $_aString[mt_rand(0, 65)];
		}
		$salt .= '$';
		return $salt;
	}
	
	private function _preLogin($user_email) {
		$this->db->from('jyc_user');
		$this->db->where('user_email', $user_email);
		$this->db->where('user_status', '2');
		return $this->db->get()->row();
	}
}