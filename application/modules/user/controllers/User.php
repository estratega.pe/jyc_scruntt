<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MX_Controller {
	
	var $args;
	var $uID;
	
	function __construct()
	{
		parent::__construct();
		$this->args['BASE_URL'] = site_url();
		$this->uID = $this->session->userdata('USERID');
		$this->load->model('dashboard/projectModel', 'project');
		$this->load->model('dashboard/activityModel', 'activity');
		$this->load->model('dashboard/conceptModel', 'concept');
		$this->load->model('user/userModel', 'home');
	}
	
	public function index() {
		$rProject = $this->project->getUhP(['jyc_user_user_id' => $this->uID]);
		$listProject = [];
		$scripts = [];
		foreach ($rProject as $p) {
			$project = $this->project->getProjectRow(['project_id' => $p->project_project_id]);
			$rActivity = $this->project->getProjectActivity(['project_project_id' => $project->project_id]);
			$listActivity = [];
			foreach ($rActivity as $a) {
				$activity = $this->activity->getActivityRow(['activity_id' => $a->activity_activity_id]);
				$concept = $this->concept->getConceptRow(['concept_id' => $a->concept_concept_id]);
				$daily_id = $project->project_id . '-' . $concept->concept_id . '-' . $activity->activity_id;
				array_push($listActivity, [
						'DAILY_ID'			=> $daily_id
						,'ACTIVITY_NAME'	=> $activity->activity_name
						,'CONCEPT_NAME'		=> $concept->concept_name
						,'DAILY_TODAY'		=> ''//date('Y-m-d', strtotime('-2 day'))
						,'DAILY_MINYEAR'	=> date('Y', strtotime('-1 year'))
						,'DAILY_MAXYEAR'	=> date('Y', strtotime('+1 year'))
				]);
				$scripts[] = '$("#daily-' . $daily_id . '").editable({mode: "inline", url:"' . base_url() . 'user/setDailyEnd/' . $project->project_id . '/' . $concept->concept_id . '/' . $activity->activity_id . '", send: "always", success: function(data) {jQuery.jGrowl(data.msg, {sticky: false,position: "top-right",theme: data.theme});}});';
			}
			array_push($listProject, [
					'PROJECT_NAME'		=> $project->project_name
					,'LIST_ACTIVITY'	=> $listActivity
			]);
		}
		$this->args['EXTRA_CSS'] = '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/widgets/jgrowl-notifications/jgrowl.css">';
		$this->args['EXTRA_SCRIPTS'] = '<script type="text/javascript" src="' . site_url() . 'assets/widgets/xeditable/xeditable.js"></script>
<script type="text/javascript" src="' . base_url() . 'assets/widgets/daterangepicker/moment.js"></script>
<script type="text/javascript" src="' . base_url() . 'assets/widgets/jgrowl-notifications/jgrowl.js"></script>
<script>
' . implode("\n",$scripts) . '
</script>
';
		$this->args['CONTENT_BODY'] = $this->parser->parse('user/home', [
				'BASE_URL'			=> base_url()
				,'LIST_PROJECT'		=> $listProject
		], true);
		$this->parser->parse('layout', $this->args);
	}
	
	public function setDailyEnd() {
		$project_id = (sizeof(func_get_args()) >= 0x0001) ? func_get_arg(0): $this->uri->segment(4);
		$concept_id = (sizeof(func_get_args()) >= 0x0002) ? func_get_arg(1): $this->uri->segment(5);
		$activity_id = (sizeof(func_get_args()) >= 0x0003) ? func_get_arg(2): $this->uri->segment(6);
		$daily_f_end = $this->input->post('value', TRUE);
		if( !is_numeric($project_id) || !is_numeric($concept_id) || !is_numeric($activity_id) ) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
			exit();
		}
		$data = [];
		$data['daily_f_end'] = $daily_f_end;
		$data['jyc_user_user_id'] = $this->uID;
		$data['project_project_id'] = $project_id;
		$data['activity_activity_id'] = $activity_id;
		
		if( $this->home->saveDaily($data) === FALSE ) {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode( ['theme' => 'bg-red', 'msg' => 'Ups!!! lo sentimos no se ha podido realizar la tarea, intentlo mas tarde.']));
		} else {
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode( ['theme' => 'bg-green', 'msg' => 'Realizado correctamente']));
		}
	}
}