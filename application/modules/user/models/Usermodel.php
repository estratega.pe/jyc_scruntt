<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userModel extends MX_Controller {
	
	const DAILY = 'daily';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function saveDaily($data) {
		$this->db->trans_begin();
		$this->db->insert(self::DAILY, $data);
		$ID = $this->db->insert_id();
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return FALSE;
		}
		else
		{
			$this->db->trans_commit();
			return $ID;
		}
	}
	
}