
	<h2>PROYECTOS A LOS QUE PERTENECES</h2>
	{LIST_PROJECT}
	<div class="content-box mrg20T">
            <h3 class="content-box-header bg-primary">
                {PROJECT_NAME}
                <span class="header-buttons-separator">
                    <a href="#" class="icon-separator toggle-button">
                        <i class="glyph-icon icon-chevron-down icon-rotate-180"></i>
                    </a>
                </span>
            </h3>
            <div class="content-box-wrapper hide">
            	<div class="form-horizontal bordered-row">
            	<div class="row">
            		<div class="col-md-2 text-center"><label class="control-label">CONCEPTO</label></div>
            		<div class="col-md-5 text-center"><label class="control-label">ACTIVIDAD</label></div>
            		<div class="col-md-5 text-center"><label class="control-label">FECHA PARA TERMINAR</label></div>
            	</div>
                {LIST_ACTIVITY}
                <div class="form-group">
                	<div class="col-md-2 text-center"><span class="bs-label label-azure">{CONCEPT_NAME}</span></div>
            		<div class="col-md-5"><label class="control-label">{ACTIVITY_NAME}</label></div>
            		<div class="col-md-5 text-center"><a href="#" id="daily-{DAILY_ID}" data-combodate='{"minYear": "{DAILY_MINYEAR}", "maxYear": "{DAILY_MAXYEAR}"}' data-type="combodate" data-value="{DAILY_TODAY}" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-title="Fecha para terminar"></a></div>
				</div>
                {/LIST_ACTIVITY}
                </div>
            </div>
        </div>
        {/LIST_PROJECT}

<script>
jQuery('.toggle-button').click(function(ev) {
	ev.preventDefault();
	jQuery(".glyph-icon", this).toggleClass("icon-rotate-180");
	jQuery(this).parents(".content-box:first").find(".content-box-wrapper").slideToggle();
	jQuery(this).parents(".content-box:first").find(".content-box-wrapper").removeClass('hide');
});
</script>