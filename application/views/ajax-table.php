<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero">
		{BODY_DESCRIPTION}
		</h3>
		<div class="example-box-wrapper">
			<table id="datatable-responsive" class="table table-striped table-bordered responsive no-wrap" cellspacing="0" width="100%">
			<thead>
				<tr>
				{TH_TABLE}
					<th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</thead>
			
			<tfoot>
				<tr>
				{TH_TABLE}
				    <th>{LABEL}</th>
				{/TH_TABLE}
				</tr>
			</tfoot>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript" src="{BASE_URL}assets/widgets/datatable/datatable.js"></script>
<script type="text/javascript">

    /* Datatables responsive */
    var Script = function () {
        jQuery('#datatable-responsive').dataTable( {
            responsive: true
            ,bProcessing: true
            ,sAjaxSource:"{URL_AJAX}"
            ,sServerMethod: "POST"
            ,bServerSide: true
            //,bStateSave: true
            ,oLanguage: {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "&Uacute;ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        	,autoWidth: false
        	,fixedColumns: true
		});

        jQuery('.dataTables_filter input').attr("placeholder", "Buscar...");
    }();

</script>
