<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="page-title">
    <h2>{BODY_TITLE}</h2>
    <p>{BODY_SUBTITLE}</p>
    {BODY_MENU}
</div>

<div class="panel">
	<div class="panel-body">
		<h3 class="title-hero">
		{BODY_DESCRIPTION}
		</h3>
		<div class="example-box-wrapper">
			<form method="post" action="{URL_POST}" class="">
				<div class="row">
                    <div class="col-md-6 form-horizontal bordered-row">
						<div class="form-group">
							<div class="col-sm-3">
							{INPUT_DELETE_ID}
							</div>
                            <div class="col-sm-9">
                                {BUTTON_SUBMIT}
                            </div>
						</div><p></p>
					</div>
					<div class="col-md-6 form-horizontal bordered-row">
						<div class="form-group">
							<div class="col-sm-6">
							
							</div>
							<div class="col-sm-6">
                                {BUTTON_CANCEL}
                            </div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
