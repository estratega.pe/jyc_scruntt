<!DOCTYPE html>
<html lang="en">
<head>

    <style>
        /* Loading Spinner */
        .spinner{margin:0;width:70px;height:18px;margin:-35px 0 0 -9px;position:absolute;top:50%;left:50%;text-align:center}.spinner > div{width:18px;height:18px;background-color:#333;border-radius:100%;display:inline-block;-webkit-animation:bouncedelay 1.4s infinite ease-in-out;animation:bouncedelay 1.4s infinite ease-in-out;-webkit-animation-fill-mode:both;animation-fill-mode:both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes bouncedelay{0%,80%,100%{-webkit-transform:scale(0.0)}40%{-webkit-transform:scale(1.0)}}@keyframes bouncedelay{0%,80%,100%{transform:scale(0.0);-webkit-transform:scale(0.0)}40%{transform:scale(1.0);-webkit-transform:scale(1.0)}}
    </style>


    <meta charset="UTF-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title> {PAGE_TITLE} </title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Favicons -->

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{BASE_URL}assets/images/icons/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{BASE_URL}assets/images/icons/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{BASE_URL}assets/images/icons/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="{BASE_URL}assets/images/icons/apple-touch-icon-57-precomposed.png">
<link rel="shortcut icon" href="{BASE_URL}assets/images/icons/favicon.png">

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/bootstrap/css/bootstrap.css">

<!-- HELPERS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/animate.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/backgrounds.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/boilerplate.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/border-radius.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/grid.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/page-transitions.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/spacing.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/typography.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/utils.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/colors.css">

<!-- ELEMENTS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/badges.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/buttons.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/content-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/dashboard-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/forms.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/images.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/info-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/invoice.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/loading-indicators.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/menus.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/panel-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/response-messages.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/responsive-tables.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/ribbon.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/social-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/tables.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/tile-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/elements/timeline.css">



<!-- ICONS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/fontawesome/fontawesome.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/linecons/linecons.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/icons/spinnericon/spinnericon.css">


<!-- WIDGETS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/accordion-ui/accordion.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/calendar/calendar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/carousel/carousel.css">

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/justgage/justgage.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/morris/morris.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/piegage/piegage.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/charts/xcharts/xcharts.css">

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/chosen/chosen.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/colorpicker/colorpicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datatable/datatable.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datepicker/datepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/datepicker-ui/datepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dialog/dialog.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dropdown/dropdown.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/dropzone/dropzone.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/file-input/fileinput.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/input-switch/inputswitch.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/input-switch/inputswitch-alt.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/ionrangeslider/ionrangeslider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jcrop/jcrop.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/jgrowl-notifications/jgrowl.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/loading-bar/loadingbar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/maps/vector-maps/vectormaps.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/markdown/markdown.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/modal/modal.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/multi-select/multiselect.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/multi-upload/fileupload.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/nestable/nestable.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/noty-notifications/noty.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/popover/popover.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/pretty-photo/prettyphoto.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/progressbar/progressbar.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/range-slider/rangeslider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/slidebars/slidebars.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/slider-ui/slider.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/summernote-wysiwyg/summernote-wysiwyg.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/tabs-ui/tabs.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/theme-switcher/themeswitcher.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/timepicker/timepicker.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/tocify/tocify.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/tooltip/tooltip.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/touchspin/touchspin.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/uniform/uniform.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/wizard/wizard.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/widgets/xeditable/xeditable.css">

<!-- SNIPPETS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/chat.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/files-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/login-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/notification-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/progress-box.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/todo.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/user-profile.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/snippets/mobile-navigation.css">

<!-- APPLICATIONS -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/applications/mailbox.css">

<!-- Admin theme -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/admin/layout.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/admin/color-schemes/default.css">

<!-- Components theme -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/components/default.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/themes/components/border-radius.css">

<!-- Admin responsive -->

<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/responsive-elements.css">
<link rel="stylesheet" type="text/css" href="{BASE_URL}assets/helpers/admin-responsive.css">

    <!-- JS Core -->

    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-core.js"></script>
    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-core.js"></script>
    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-widget.js"></script>
    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-mouse.js"></script>
    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-ui-position.js"></script>
    <!-- <script type="text/javascript" src="{BASE_URL}assets/js-core/transition.js"></script>-->
    <script type="text/javascript" src="{BASE_URL}assets/js-core/modernizr.js"></script>
    <script type="text/javascript" src="{BASE_URL}assets/js-core/jquery-cookie.js"></script>





    <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function() {
                $('#loading').fadeOut( 400, "linear" );
            }, 300);
        });
    </script>



</head>


<body class="closed-sidebar">
    <div id="sb-site">

    <div id="loading">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <div id="page-wrapper">
        <div id="page-header" class="bg-gradient-9">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="{BASE_URL}" class="logo-content-small" title="MonarchUI"></a>
    </div>
    <div id="header-logo" class="logo-bg">
        <a href="{BASE_URL}" class="logo-content-big" title="MonarchUI">
            Monarch <i>UI</i>
            <span>The perfect solution for user interfaces</span>
        </a>
        <a href="{BASE_URL}" class="logo-content-small" title="MonarchUI">
            Monarch <i>UI</i>
            <span>The perfect solution for user interfaces</span>
        </a>
        <!-- 
        <a id="close-sidebar" href="#" title="Close sidebar">
            <i class="glyph-icon icon-angle-left"></i>
        </a>-->
    </div>
    <div id="header-nav-left">
        <div class="user-account-btn dropdown">
            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                <img width="28" src="{BASE_URL}assets/image-resources/gravatar.jpg" alt="Profile image">
                <span>Jose Gonzales</span>
                <i class="glyph-icon icon-angle-down"></i>
            </a>
            <div class="dropdown-menu float-left">
                <div class="box-sm">
                    <div class="login-box clearfix">
                        <div class="user-img">
                            <a href="#" title="" class="change-img">Change photo</a>
                            <img src="{BASE_URL}assets/image-resources/gravatar.jpg" alt="">
                        </div>
                        <div class="user-info">
                            <span>
                                Jose Gonzales
                                <i>IT Consultant</i>
                            </span>
                            <a href="#" title="Edit profile">Edit profile</a>
                            <a href="#" title="View notifications">View notifications</a>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <ul class="reset-ul mrg5B">
                        <li>
                            <a href="{BASE_URL}dashboard/project">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Proyectos/Contratos
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/platform">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Plataforma
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/concept">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Concepto
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/activity">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Actividad
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/activity-type">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Tipo de Actividad
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/customer">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Clientes
                            </a>
                        </li>
                        <li>
                            <a href="{BASE_URL}dashboard/user">
                                <i class="glyph-icon float-right icon-caret-right"></i>
                                Usuarios
                            </a>
                        </li>
                    </ul>
                    <div class="pad5A button-pane button-pane-alt text-center">
                        <a href="{BASE_URL}login/loginOut" class="btn display-block font-normal btn-danger">
                            <i class="glyph-icon icon-power-off"></i>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #header-nav-left -->

    <div id="header-nav-right">
    {PAGE_HEADER_NAV}
    </div><!-- #header-nav-right -->

</div>
        <!-- <div id="page-sidebar">
		    <div class="scroll-sidebar">
		    {PAGE_SIDEBAR_MENU}
		    </div>
		</div> -->
        <div id="page-content-wrapper">
            <div id="page-content">
                    <div class="container">
						{CONTENT_BODY}
                    </div>
            </div>
        </div>
    </div>


    <!-- WIDGETS -->
<script type="text/javascript" src="{BASE_URL}assets/tether/js/tether.js"></script>
<script type="text/javascript" src="{BASE_URL}assets/bootstrap/js/bootstrap.js"></script>
{EXTRA_SCRIPTS}



<!-- Content box -->

<!-- <script type="text/javascript" src="{BASE_URL}assets/widgets/content-box/contentbox.js"></script>-->
</div>
</body>
</html>